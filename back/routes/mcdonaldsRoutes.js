const mcdonaldsRoutes = require('express').Router();

/** get methods in mcdonalds controller */
const { getMcdonalds } = require('../controllers/mcdonalds.controller');

/** mcdonalds api */
mcdonaldsRoutes.route('/:code').get(getMcdonalds);

module.exports = mcdonaldsRoutes;
