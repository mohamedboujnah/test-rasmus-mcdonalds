const routes = require('express').Router();
const mcdonalds = require('./mcdonaldsRoutes');

/** GET /api page. */
routes.get('/', function(req, res, next) {
    res.json({status:"success", message:"Hello from API", data:{}})
});

/** mcdonalds routes */
routes.use('/mcdonalds', mcdonalds);

module.exports = routes;
