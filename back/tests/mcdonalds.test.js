const request = require('supertest')
const app = require('../app');

const fs = require('fs');
var path = require('path');

describe('check the api base url', () => {
    it('should return 200 code', async () => {
        const res = await request(app).get('/api')
        expect(res.statusCode).toEqual(200)
    })
});

describe('get mcdonalds list', () => {

    // mcdonald object form
    const mcdonaldData = { 
        "lng" : expect.any(Number),
        "lat" : expect.any(Number),
        "name" : expect.any(String),
        "code" : expect.any(String),
        "adress" : expect.any(String),
        "city" : expect.any(String),
        "tel" : expect.any(String)
    };

    it('should return data property', async () => {
        const res = await request(app)
          .get('/api/mcdonalds/test')
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty('data')
      });
      
    it('should return data with key and value', async () => {
        const res = await request(app)
            .get('/api/mcdonalds/wa')
        expect(res.statusCode).toEqual(200);
        // test if the object contain all keys
        expect(res.body.data).toEqual( 
            expect.arrayContaining([ 
              expect.objectContaining(mcdonaldData)
            ])
          )

    })

});

describe('check csv file', () => {
    // path of the csv file
    var inputFile='./uploads/mcdonalds.csv';
    
    it('should return true if file exist', () => {
        const checkFile = fs.existsSync(inputFile);
        expect(checkFile).toEqual(true);
    });

    it('should return .csv', () => {
        const extension = path.extname(inputFile);
        expect(extension).toEqual(".csv");
      });

});