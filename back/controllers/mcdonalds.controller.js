const fs = require('fs');
const parse = require('csv-parse')

/**
 * add macdonals by the state code
 */
exports.getMcdonalds = (req, res) => {
    
    let mcdonaldsData = [];
    
    // get code state from params 
    let codeParams = req.params.code;

    // path of the csv file
    var inputFile='./uploads/mcdonalds.csv';

    try{
        var parser = parse(function (err, data) {

            if(err)
                return res.status(500).json({
                    error: true,
                    status:"error",
                    message:"there is an error",
                    data:{"error":err}
                });

            // Browse the parsed data    
            data.forEach(function(line) {
                // split the data by comma separator 
                let adresses = line[0].replace(/"/g, "").split(",");

                // create a new object with the data returned from the split function 
                var mcdonaldData = { 
                    "lng" : parseFloat(adresses[0]),
                    "lat" : parseFloat(adresses[1]),
                    "name" : adresses[2],
                    "code" : adresses[3],
                    "adress" : adresses[4],
                    "city" : adresses[5],
                    "tel" : adresses[7]
                };
                mcdonaldsData.push(mcdonaldData)
            });    
            
            // filter the data with the state code params 
            let filteredData = mcdonaldsData.filter(d => d.code == codeParams.toUpperCase())

            return res.status(200).json({status:"success", message:"get data for the code " + codeParams, data: filteredData})
        });

        // read the inputFile, feed the contents to the parser
        fs.createReadStream(inputFile).pipe(parser);

    }catch(err){
        return res.status(500).json({
            error: true,
            status:"error",
            message:"there is an error",
            data:{"error":err}
        });
    }
};

