const express 		= require('express');
const bodyParser 	= require('body-parser');

const routes    = require('./routes');
const app   = express();

const cors = require('cors');

// get environment from the file .env
const environment = process.env.NODE_ENV || 'dev';

app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json())

app.use('/api', routes);

app.use('/', function(req, res){
	res.statusCode = 200;//send the appropriate status code
  res.json({status:"success", message:"test racemus consulting mcdonalds", data:{}})
});

// development error handler
// will print stacktrace
if (environment === 'dev') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


//This is here to handle all the uncaught promise rejections
process.on('unhandledRejection', error => {
  console.error('Uncaught Error', error);
});

module.exports = app;