/**
 * @name McdoPage
 * @summary the McdoPage
 */

import React, { useState } from 'react';

import MapWithAMarker from './MapWithMarker';
import McdoBlock from './McdoBlockComponent';
import { mcdoServices } from '../services/mcdoServices';

const MAP_URL = "https://maps.googleapis.com/maps/api/js";

function McdoPage() {
    // init data && setData using react hooks
    let [data, setData] = useState([]);
    let [code, setCode] = useState("");

    // get the data by code && set it to the state
    const chageCode = (code) => {
        setCode(code)
    
        code && mcdoServices.getMcdoByCode(code).then(d => setData(d.data));
    }

    return (
        <div className="container">
            <input type="text" name="code" placeholder="Mettez ici votre code d'état ..." onChange={e => chageCode(e.target.value)} />
            {/* display map with marker & the data block */}
            <div>
                <MapWithAMarker
                    googleMapURL={ MAP_URL }
                    loadingElement={<div style={{ height: `100%` }} />}
                    containerElement={<div style={{ height: `400px` }} />}
                    mapElement={<div style={{ height: `100%` }} />}
                    dataMarker={data} />

                    <McdoBlock data={data} code={code}/>
                </div>
        </div>
    );
}

export default McdoPage;
