/**
 * @name McdoBlock
 * @summary the block that display the data
 */

import React from 'react';

const McdoBlock = (props) => (
    <div className="block">
      {   // error message
          (props.code && props.data.length === 0) && <div style={{ color: "red"}}>Aucune donnée trouvée, veuillez entrer un autre code d'état</div>
      }
      {(props.data || []).map((d, index) => (
        <div key={index}>{ `${d.name}, ${d.code}, ${d.adress}, ${d.city}, ${d.tel}` }</div>
      ))}
    </div>        
);

export default McdoBlock;
