/**
 * @name MapWithAMarker
 * @summary the Map with the markers of Mcdo
 */
import React from 'react';
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker,
  } from "react-google-maps";

const MapWithAMarker = withScriptjs(withGoogleMap(props => (
    props.dataMarker.length > 0 &&
        <GoogleMap
            defaultZoom={7}
            defaultCenter={{ lat: props.dataMarker[0].lat, lng: props.dataMarker[0].lng }}>  
            
            {(props.dataMarker || []).map((d, index) => (
                    <Marker key={index} position={{ lat: d.lat, lng: d.lng }} />
            ))}            
        </GoogleMap>
  )));

  
export default MapWithAMarker;