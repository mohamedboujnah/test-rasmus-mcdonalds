import React from 'react';
import './App.css';

import McdoPage from './components/McdoPage';

function App() {
  return (
    <McdoPage />
  );
}

export default App;
