export const mcdoServices = {
    getMcdoByCode,
};

let BASE_URL = 'http://localhost:8000/api/';

/**
 * get mcdo data by state code
 */
function getMcdoByCode(code) {

    return fetch(BASE_URL + 'mcdonalds/'+ code)
    .then(response =>  response.json())
    .then(resData => {
        return resData;       
    }).catch(function(err) {
        console.log(err);
    });
        
}